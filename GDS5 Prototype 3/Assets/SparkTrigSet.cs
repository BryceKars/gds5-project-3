﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkTrigSet : MonoBehaviour {

    public GameObject Explosion;
    public GameObject Fridge;

    public AudioSource ExplosionSource;
    public AudioClip ExplosionClip;

    public AudioSource SparkSource;
    public AudioClip SparkClip;

    public AudioSource NewSparkSource;
    public AudioClip NewSparkClip;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SparkTrue()
    {
        Explosion.GetComponent<ParticleSystem>().Emit(30);
        Fridge.gameObject.SetActive(false);
        ExplosionSource.PlayOneShot(ExplosionClip);
        GetComponent<AudioSource>().Stop();
        
    }

    public void PlayNewSpark()
    {
        NewSparkSource.PlayOneShot(NewSparkClip);

    }
}
