﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedText : MonoBehaviour {

    public string message;
    public Text messageText;
    public WhatShouldISay iSay;
    public bool TextAnimating;
    public GameObject GhostTextBox;

    // Use this for initialization
    void Start ()
    {
        messageText.GetComponent<Text>();
        messageText.text = iSay.Say;
        message = messageText.text;
        messageText.text = "";
    }
	
	void Update ()
    {

    }

    IEnumerator AnimateText()
    {
        TextAnimating = true;
        GetComponent<Animator>().SetBool("TextTriggered", true);
        GhostTextBox.GetComponent<Animator>().SetBool("TextTriggered", true);
        yield return null;
        GetComponent<Animator>().SetBool("TextTriggered", false);
        GhostTextBox.GetComponent<Animator>().SetBool("TextTriggered", false);

        foreach (char letter in message.ToCharArray())
        {
            messageText.text += letter;
            yield return 0;
            yield return new WaitForSeconds(iSay.letterTime);
        }
        TextAnimating = false;

    }

    public void Changetext()
    {
        message = messageText.text;
        messageText.text = "";
    }
}
