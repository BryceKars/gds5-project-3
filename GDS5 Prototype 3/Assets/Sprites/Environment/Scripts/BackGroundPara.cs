﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundPara : MonoBehaviour {

    public Transform Dog;

    public float OffsetX;
    public float OffsetY;

    public float BGSpeed;


	// Use this for initialization
	void Start ()
    {
        
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        OffsetX = Dog.transform.localPosition.x * BGSpeed;
        OffsetY = Dog.transform.localPosition.y * BGSpeed;

        gameObject.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(OffsetX, OffsetY));
    }
}
