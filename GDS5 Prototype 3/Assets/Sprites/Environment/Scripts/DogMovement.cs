﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Animations;

public class DogMovement : MonoBehaviour
{

    public Transform MouseMarker;

    Animator anim;
    Rigidbody2D rb;

    public float Speed;
    public float topSpeed;

    public Sprite FallingDoggo;
    public AnimatedText animatedText;
    public StaticObjectScript staticObjectScript;
    public GameObject GhostText;
    public GameObject GhostTextBox;
    public GameObject TextTrigger;
    public GameObject iSay;
    public GameObject RodentArrow;
    public GameObject Ramp;
    public AnimationClip Turning;

    public AudioSource DogWalkSource;
    public AudioClip DogWalkClip;
    public AudioClip DogGrowlClip;
    public AudioClip DogBarkClip;

    public AudioSource DingSource;
    public AudioClip DingClip;

    public bool grounded = false;
    public bool walking = false;
    public Transform GroundCheck;
    float GroundRadius = 0.15f;
    public LayerMask WhatIsGround;
    public LayerMask WhatIsDog;
    public LayerMask WhatIsEnemy;

    public Rodent01 rodent01;
    public MouseDeadZoneTrigger mouseDead;
    public SmoothCamera smoothCamera;

    public GameObject WheelBin;

    public Animator FadeText;
    public GameObject whatShouldISay;
    public GameObject HintBundle;


    public bool FacingLeft;

    // Use this for initialization
    void Start()
    {
        iSay = null;
        DogWalkSource.clip = DogWalkClip;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        Physics.queriesHitTriggers = false;
        FacingLeft = true;
        walking = false;
        whatShouldISay = null;
        grounded = false;
        FadeText = GhostText.GetComponent<Animator>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }

    void Update()
    {

        if (staticObjectScript.StaticTriggered == false)
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), Ramp.GetComponent<Collider2D>(), true);
        }
        else
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), Ramp.GetComponent<Collider2D>(), false);
        }


        //if (mouseDead.GetComponent<MouseDeadZoneTrigger>().MoveDisabled == false && rodent01.GrowlActive == false && rodent01.DogNear == false)
        {
            //if (Input.GetKey(KeyCode.Mouse1) && (gameObject.tag == "Possessed") && FacingLeft == true && grounded)
            if (Input.GetKey(KeyCode.D) && grounded)
            {
                rb.AddForce(Vector2.right * Speed, ForceMode2D.Impulse);
                walking = true;
                anim.SetBool("CorgiWalking", true);
            }

            //if (Input.GetKey(KeyCode.Mouse1) && (gameObject.tag == "Possessed") && FacingLeft == false && grounded)
            if (Input.GetKey(KeyCode.A) && grounded)
            {
                rb.AddForce(Vector2.right * -Speed, ForceMode2D.Impulse);
                walking = true;
                anim.SetBool("CorgiWalking", true);
            }
        }

        if (/*gameObject.tag == "Possessed" &&*/ mouseDead.MoveDisabled == false)
        {
            //if (MouseMarker.position.x < transform.position.x)
            if (Input.GetKey(KeyCode.A))
            {
                if (FacingLeft == true)
                {
                    StartCoroutine(TurningTriggerLeft());
                    FacingLeft = false;
                    GroundCheck.position = transform.position + new Vector3(0, -1.15f, 0);
                }

            }

            //if (MouseMarker.position.x > transform.position.x)
            if (Input.GetKey(KeyCode.D))
            {
                if (FacingLeft == false)
                {
                    StartCoroutine(TurningTriggerRight());
                    FacingLeft = true;
                    GroundCheck.position = transform.position + new Vector3(0, -1.15f, 0);
                }
            }
        }






        if (grounded != true)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                GroundCheck.position = transform.position + new Vector3(0, -1.15f, 0);
                grounded = true;
            }

            GetComponent<Rigidbody2D>().gravityScale = 2f;

        }

        if (rodent01.DogNear == true && rodent01.GrowlActive == true && walking == false)
        {
            DogBark();
        }

        grounded = Physics2D.OverlapCircle(GroundCheck.position, GroundRadius, WhatIsGround);
        anim.SetBool("Ground", grounded);

        Physics2D.IgnoreCollision(WheelBin.GetComponent<Collider2D>(), GetComponent<Collider2D>());

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }



        if (grounded)
        {
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.down, 1f, WhatIsGround);
            transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
            GetComponent<Rigidbody2D>().gravityScale = 1;
        }
        else
        {
            return;
        }

        //if (Input.GetKeyUp(KeyCode.Mouse1))
        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            walking = false;
        }

        if (rb.velocity.magnitude > topSpeed)
        {
            rb.velocity = GetComponent<Rigidbody2D>().velocity.normalized * topSpeed;
        }

        if (walking != true)
        {
            rb.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && (gameObject.tag == "Possessed"))
        {
            DogWalkSource.Play();
        }

        if (rb.velocity.magnitude > 1)
        {
            anim.SetBool("CorgiWalking", true);
        }
        else if (rb.velocity.magnitude < 1)
        {
            anim.SetBool("CorgiWalking", false);
        }
    }


    public void DogBark()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity, WhatIsEnemy);

        //if (Input.GetMouseButtonDown(1) && hit.collider != null)
        if(Input.GetKeyDown(KeyCode.E)/* && hit.collider != null*/)
        {
            //Debug.Log(hit.collider.name);
            StartCoroutine(BarkTrigger());
            rodent01.StartCoroutine("IdleRetrigger");
            if (RodentArrow.GetComponent<SpriteRenderer>().enabled == true)
            {
                RodentArrow.GetComponent<SpriteRenderer>().enabled = false;
            }
           

        }
        else
        {
            return;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 18 && animatedText.TextAnimating == false)
        {
            whatShouldISay = collision.gameObject;
            DingSource = collision.GetComponent<AudioSource>();
            iSay = collision.gameObject;
            collision.enabled = false;
            collision.GetComponent<ParticleSystem>().Stop();
            collision.GetComponentInChildren<Light>().enabled = false;
            GhostText.GetComponent<Animator>().SetFloat("AnimSpeed", whatShouldISay.GetComponent<WhatShouldISay>().AnimTime);
            GhostTextBox.GetComponent<Animator>().SetFloat("AnimSpeed", whatShouldISay.GetComponent<WhatShouldISay>().AnimTime);
            GhostText.GetComponent<Text>().text = iSay.GetComponent<WhatShouldISay>().Say;
            animatedText.Changetext();
            animatedText.StartCoroutine("AnimateText");
            DingSource.PlayOneShot(DingClip);

            if (iSay.gameObject.GetComponent<WhatShouldISay>().CameraPan == true)
            {
                StartCoroutine(CameraPanObject());
            }

        }
        else
        {
            return;
        }
    }

    public void PlayWalkSound()
    {
        DogWalkSource.PlayOneShot(DogWalkClip);
    }

    public void PlayGrowlSound()
    {
        DogWalkSource.PlayOneShot(DogGrowlClip);
    }
    public void PlayBarkSound()
    {
        DogWalkSource.PlayOneShot(DogBarkClip);
    }

    IEnumerator BarkTrigger()
    {
        rodent01.GetComponent<Animator>().SetBool("DogBarked", true);
        GetComponent<Animator>().SetBool("Barking", true);
        yield return new WaitForSeconds(0.02f);
        rodent01.GetComponent<Animator>().SetBool("DogBarked", false);
        GetComponent<Animator>().SetBool("Barking", false);


    }

    IEnumerator TurningTriggerRight()
    {
        if (FacingLeft == false)
        {
            GetComponent<Animator>().SetBool("Turning", true);
            yield return new WaitForFixedUpdate();
            GetComponent<Animator>().SetBool("Turning", false);
            yield return new WaitForSeconds(Turning.length);
            GetComponent<SpriteRenderer>().flipX = false;


        }

    }
    IEnumerator TurningTriggerLeft()
    {
        if (FacingLeft == true)
        {
            GetComponent<Animator>().SetBool("Turning", true);
            yield return new WaitForFixedUpdate();
            GetComponent<Animator>().SetBool("Turning", false);
            yield return new WaitForSeconds(Turning.length);
            GetComponent<SpriteRenderer>().flipX = true;
            

        }
    }

    IEnumerator CameraPanObject()
    {
        smoothCamera.player2 = whatShouldISay.GetComponent<WhatShouldISay>().TargetPan.transform;
        yield return new WaitForSeconds(5f);
        smoothCamera.player2 = transform;
    }
}