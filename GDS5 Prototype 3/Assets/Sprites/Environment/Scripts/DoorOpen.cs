﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour {

    public AudioSource DoorOpenSource;
    public AudioClip DoorOpenClip;

    public GameObject Door;

	// Use this for initialization
	void Start ()
    {
        DoorOpenSource.clip = DoorOpenClip;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Possessed")
        {
            Door.GetComponent<Animator>().SetTrigger("DoorTrigger");
            DoorOpenSource.Play();
            gameObject.SetActive(false);
            
        }
    }
}
