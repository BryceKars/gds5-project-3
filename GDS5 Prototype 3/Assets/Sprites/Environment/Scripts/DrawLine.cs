﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour {

    public Transform Piston;
    public Transform PistonBase;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<LineRenderer>().SetPosition(1, new Vector3(Piston.transform.position.x, Piston.transform.position.y, Piston.transform.position.z));
        GetComponent<LineRenderer>().SetPosition(0, new Vector3(PistonBase.transform.position.x, PistonBase.transform.position.y, PistonBase.transform.position.z));

    }
}
