﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMoveScript : MonoBehaviour {

    public float speed;
    public GameObject pointA;
    public GameObject pointB;
    public GameObject Ghost;
    public bool reverseMove;
    public float FindTime;

    public AudioSource EnemSource;
    public AudioClip EnemClip;

    public Collider2D TrashBin;

    private Vector3 velocity = Vector3.zero;

    private bool FoundPlayer;
    public bool AttackPlayer;

    // Use this for initialization
    void Start()
    {
        FindTime = 1.5f;
        FoundPlayer = false;
        EnemSource.clip = EnemClip;
        reverseMove = false;
        
    }

    // Update is called once per frame
    void Update()
    {

        Physics2D.IgnoreLayerCollision(8, 9);


        if (FoundPlayer == false && AttackPlayer == false)
        {
            if (reverseMove == true)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(pointA.transform.position.x,pointA.transform.position.y),speed);
                GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(pointB.transform.position.x, pointB.transform.position.y),speed);
                GetComponent<SpriteRenderer>().flipX = false;
            }

            if ((Vector2.Distance(transform.position, pointA.transform.position) == 0.0f) || Vector2.Distance(transform.position, pointB.transform.position) == 0.0f)
            {
                if (reverseMove == true)
                {
                    reverseMove = false;
                }
                else
                {
                    reverseMove = true;
                }
                
            }
        }

        if (FoundPlayer == true)
        {

            EnemSource.Play();
            FindTime -= Time.deltaTime;
            GetComponent<Animator>().SetBool("EnemAlerted", true);

            if (Ghost.transform.position.x < transform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = true;
            }

            if (Ghost.transform.position.x > transform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = false;
            }

            if (FindTime <= 0f)
            {
                AttackPlayer = true;
            }
        }

        if (AttackPlayer == true)
        {
            transform.position = Vector3.SmoothDamp(transform.position, Ghost.transform.position, ref velocity, 0.5f, 8f, Time.deltaTime);
            GetComponent<CircleCollider2D>().radius = 25;


            GetComponent<Animator>().SetBool("EnemAttack", true);

            if (Ghost.transform.position.x < transform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = true;
            }

            if (Ghost.transform.position.x > transform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = false;
            }
        }
        
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Ghost")
        {
            FoundPlayer = true;
            Debug.Log(FoundPlayer);
        }
        else
        {
            FoundPlayer = false;
            GetComponent<Animator>().SetBool("EnemAlerted", false);
            FindTime = 1.5f;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ghost")
        {
            GetComponent<Animator>().SetBool("EnemAlerted", false);
            FoundPlayer = false;
            AttackPlayer = false;
            FindTime = 1.5f;
        }

        if (collision.gameObject.layer == 10)
        {
            AttackPlayer = false;
        }
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if ((other.gameObject.tag == "Ghost" || other.gameObject.tag == "Hidden") && AttackPlayer == true)
        {
            Debug.Log("Collided");
            SceneManager.LoadScene(0);
        }
    }

}
