﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GhostPos : MonoBehaviour {

    public Transform ParentObject;
    public Transform CurrentTarget;

    public AudioSource GhostPosSource;
    public AudioClip GhostPosClip;

    public DogMovement DogMovement;
    public float DogSide;

    public LayerMask Possessable;

    public GameObject OldPossess;
    public GameObject TextPossess;
    public GameObject GhostTalkText;
    public GameObject Flame;
    public GameObject Dog;
    public bool FacingLeft;
    public AnimationClip Turning;

    private Vector3 velocity = Vector3.zero;



    // Use this for initialization
    void Start ()
    {
        CurrentTarget.transform.position = gameObject.transform.position;
        GhostPosSource.clip = GhostPosClip;
        DogSide = 1f;
	}
	
	// Update is called once per frame
	void Update ()
    {

        GhostTalkText.gameObject.transform.position = Camera.main.WorldToScreenPoint(transform.position) + new Vector3(0, 200, 0);
        TextPossess.GetComponent<Text>().text = ParentObject.name.ToString();

        Flame.gameObject.transform.position = ParentObject.position + new Vector3(0,2,0);


        if (ParentObject.name == "Dog")
        {
            transform.position = Vector3.SmoothDamp(CurrentTarget.transform.position, ParentObject.transform.position + new Vector3(DogSide, 2f, 0), ref velocity, 0.5f);
        }
        else
        {
            transform.position = Vector3.SmoothDamp(CurrentTarget.transform.position, ParentObject.transform.position + new Vector3(0f, 2f, 0f), ref velocity, 0.5f);
        }
        

        if (DogMovement.FacingLeft == true)
        {
            
            DogSide = -1f;
        }
        else if (DogMovement.FacingLeft == false)
        {
            DogSide = 1f;
        }


        if (ParentObject.position.x < transform.position.x)
        {
            if (FacingLeft == true)
            {
                StartCoroutine(TurningTriggerLeft());
                FacingLeft = false;
            }
            
        }

        if (ParentObject.position.x > transform.position.x)
        {
            if (FacingLeft == false)
            { 
                StartCoroutine(TurningTriggerRight());
                FacingLeft = true;
            }

        }


        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity, Possessable);

            if ((hit.collider != null) && (hit.collider.tag != "Untagged") && (hit.collider.tag != "Enemy") && (hit.collider.tag != "Breakable"))
            {
                OldPossess = GameObject.FindGameObjectWithTag("Possessed");
                OldPossess.gameObject.tag = "Possess";

                Debug.Log(hit.transform.name);
                if ((hit.collider.tag == "Possess") && (hit.collider.tag != "Possessed"))
                {
                    if (hit.collider.name != "Dog")
                    {
                        GhostPosSource.Play();
                        GetComponent<Animator>().SetTrigger("GhostPossessing");
                        ParentObject = hit.transform;
                        hit.collider.transform.tag = "Possessed";
                        StartCoroutine(SpriteDisable());
                    }
                    else
                    {
                        GhostPosSource.Play();
                        GetComponent<Animator>().SetTrigger("GhostPossessing");
                        ParentObject = hit.transform;
                        hit.collider.transform.tag = "Possessed";
                        StartCoroutine(SpriteEnabled());

                    }

                }
                else
                {
                    return;
                }
            }
            else
            {
                OldPossess = GameObject.FindGameObjectWithTag("Possessed");
                OldPossess.gameObject.tag = "Possess";

                GhostPosSource.Play();
                GetComponent<Animator>().SetTrigger("GhostPossessing");
                ParentObject = Dog.transform;
                Dog.transform.tag = "Possessed";
                StartCoroutine(SpriteEnabled());

                
            }
        }

    }

    IEnumerator SpriteDisable()
    {
        GetComponentInChildren<Light>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        GetComponent<SpriteRenderer>().enabled = false;
        Flame.GetComponent<SpriteRenderer>().enabled = true;
        
    }

    IEnumerator SpriteEnabled()
    {
        Flame.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponentInChildren<Light>().enabled = true;


    }

    IEnumerator TurningTriggerRight()
    {
        if (FacingLeft == false)
        {
            GetComponent<Animator>().SetBool("Turning", true);
            yield return new WaitForEndOfFrame();
            GetComponent<Animator>().SetBool("Turning", false);
            yield return new WaitForSeconds(Turning.length);
            GetComponent<SpriteRenderer>().flipX = false;


        }

    }
    IEnumerator TurningTriggerLeft()
    {
        if (FacingLeft == true)
        {
            GetComponent<Animator>().SetBool("Turning", true);
            yield return new WaitForEndOfFrame();
            GetComponent<Animator>().SetBool("Turning", false);
            //yield return new WaitForSeconds(0.1666666666666667f);
            yield return new WaitForSeconds(Turning.length);
            GetComponent<SpriteRenderer>().flipX = true;


        }
    }
}
