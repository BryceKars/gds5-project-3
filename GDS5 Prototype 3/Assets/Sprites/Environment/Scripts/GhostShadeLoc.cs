﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostShadeLoc : MonoBehaviour {

    public GhostPos Parent;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Animator>().SetBool("Fade", true);
        transform.position = Parent.ParentObject.transform.position;

	}
}
