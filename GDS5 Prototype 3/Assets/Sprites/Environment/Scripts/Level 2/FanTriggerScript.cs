﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanTriggerScript : MonoBehaviour {

    public Rodent01 rodent;
    public GameObject FanSwitch;
    public GameObject Fan;
    public GameObject FanBlades;

    public AudioSource PowerDownSource;
    public AudioClip PowerDownClip;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (rodent.GetComponent<Rodent01>().FanTrig == true)
        {
            GetComponent<Animator>().SetBool("Light", true);
            FanSwitch.GetComponent<Animator>().SetBool("Fan", true);
            Fan.GetComponent<Collider2D>().enabled = false;
            FanBlades.GetComponent<Animator>().SetBool("FanActive", false);
            GetComponentInChildren<Light>().enabled = false;

        }
		
	}

    public void PlayPowerDown()
    {
        PowerDownSource.PlayOneShot(PowerDownClip);
        Fan.GetComponent<AudioSource>().Stop();
    }
}
