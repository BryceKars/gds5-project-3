﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookPulleyScript : MonoBehaviour {

    public GameObject Hook;
    public GameObject Crowbar;
    public MovingPossess movingPossess;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<LineRenderer>().SetPosition(0, new Vector3(transform.position.x, transform.position.y + 0.6f, transform.position.z));
        GetComponent<LineRenderer>().SetPosition(1, new Vector3(Hook.transform.position.x, Hook.transform.position.y, Hook.transform.position.z));

        if (GetComponent<DistanceJoint2D>().distance >= 12f)
        {
            GetComponent<DistanceJoint2D>().distance = 12f;
        }

        if ((Input.GetKey(KeyCode.Mouse1)) && (Input.GetAxis("Mouse Y") < 0) && gameObject.tag == "Possessed")
        {
            GetComponent<DistanceJoint2D>().distance += 0.1f;
        }

        if ((Input.GetKey(KeyCode.Mouse1)) && (Input.GetAxis("Mouse Y") > 0) && gameObject.tag == "Possessed")
        {
            GetComponent<DistanceJoint2D>().distance -= 0.1f;
        }
       
    }
}
