﻿using System.Collections;
using UnityEngine;

public class PistonPushScript : MonoBehaviour {


	// Use this for initialization
	void Start ()
    {
        GetComponent<Animator>().SetBool("PistonActive", false);
	}
	
	// Update is called once per frame
	public void Update ()
    {
        Piston();
    }

    void Piston()
    {
        if (Input.GetMouseButtonDown(1) && (gameObject.tag == "Possessed"))
        {
            StartCoroutine(PistonTrigger());
        }
    }

    IEnumerator PistonTrigger()
    {
        GetComponent<Animator>().SetBool("PistonActive", true);
        yield return null;
        GetComponent<Animator>().SetBool("PistonActive", false);

    }
}
