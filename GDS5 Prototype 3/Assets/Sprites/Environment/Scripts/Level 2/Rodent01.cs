﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rodent01 : MonoBehaviour
{

    public RodentExitTrigger rodentExitTrigger;
    public DogMovement dogMovement;
    public GameObject Drum;
    public bool DogNear;
    public bool FanTrig;
    public bool GrowlActive;
    public GameObject RodentArrow;
    public GameObject RodentIdle;
    public SmoothCamera smoothcamera;
    public GameObject RodentTarget;
    public GameObject Ramp;

    // Use this for initialization
    void Start()
    {
        GetComponent<Animator>().SetBool("PathOpen", true);
        FanTrig = false;
        GrowlActive = false;
        GetComponent<SpriteRenderer>().enabled = false;
        RodentIdle.GetComponent<SpriteRenderer>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        RodentIdle.transform.position = transform.position;

        if (rodentExitTrigger.GetComponent<RodentExitTrigger>().pathblocked == true)
        {
            GetComponent<Animator>().SetBool("PathOpen", false);
        }

        if (rodentExitTrigger.GetComponent<RodentExitTrigger>().pathblocked == false)
        {
            GetComponent<Animator>().SetBool("PathOpen", true);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Dog")
        {
            Debug.Log("hello");
            DogNear = true;
            GrowlActive = true;
            if (RodentArrow.GetComponent<SpriteRenderer>().enabled == false)
            {
                RodentArrow.GetComponent<SpriteRenderer>().enabled = true;
            }
            
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Dog")
        {
            Debug.Log("hello2");
            DogNear = false;
            GrowlActive = false;
            dogMovement.GetComponent<Animator>().SetBool("Growling", false);
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (dogMovement.walking == false && GrowlActive == true)
        {
            dogMovement.GetComponent<Animator>().SetBool("Growling", true);
        }
        else
        {
            dogMovement.GetComponent<Animator>().SetBool("Growling", false);
        }

        if (dogMovement.walking == true)
        {
            dogMovement.GetComponent<Animator>().SetBool("Growling", false);
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == Drum)
        {
            dogMovement.StartCoroutine("BarkTrigger");
            if (RodentArrow.GetComponent<SpriteRenderer>().enabled == true)
            {
                RodentArrow.GetComponent<SpriteRenderer>().enabled = false;
            }
           
        }
    }

    public void FanTriggered()
    {
        FanTrig = true;
    }

    IEnumerator IdleRetrigger()
    {
        if (GetComponent<SpriteRenderer>().enabled == false)
        {
            GetComponent<SpriteRenderer>().enabled = true;
        }
        RodentIdle.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(4f);
        GetComponent<SpriteRenderer>().enabled = false;
        RodentIdle.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void RodentCamera()
    {
        dogMovement.whatShouldISay = gameObject;
        smoothcamera.player2 = RodentTarget.transform;
        dogMovement.StartCoroutine("CameraPanObject");
        Ramp.GetComponent<Rigidbody2D>().isKinematic = false;
    }
}
