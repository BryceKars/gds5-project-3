﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodentExitTrigger : MonoBehaviour {

    public bool pathblocked;

	// Use this for initialization
	void Start ()
    {
        pathblocked = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "WheelBin")
        {
            pathblocked = true;
        }
        else
        {
            pathblocked = false;
        }
    }
}
