﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValvePossess : MonoBehaviour {

    public GameObject Platform;
    public StaticObjectScript staticObjectScript;


	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
            if ((Input.GetKey(KeyCode.Mouse1)) && (Input.GetAxis("Mouse X") > 0) && gameObject.tag == "Possessed")
            {
                gameObject.transform.Rotate(new Vector3(0, 0, -10), Time.deltaTime * 150f);
                if (staticObjectScript.InValve == false)
                    Platform.transform.position += new Vector3(0.05f, 0, 0);

            }

            if ((Input.GetKey(KeyCode.Mouse1)) && (Input.GetAxis("Mouse X") < 0) && gameObject.tag == "Possessed")
            {
                gameObject.transform.Rotate(new Vector3(0, 0, 10), Time.deltaTime * 150f);
                if (staticObjectScript.InValve == false)
                    Platform.transform.position += new Vector3(-0.05f, 0, 0);
            }

            if (Platform.transform.position.x < 39.5f)
            {
                Platform.transform.position = new Vector3(39.5f, 3.1f, 0);
            }

            if (Platform.transform.position.x > 43.9f)
            {
                Platform.transform.position = new Vector3(43.9f, 3.1f, 0);
            }
    }

        
}
