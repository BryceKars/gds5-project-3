﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZapWireTrigger : MonoBehaviour {

    public GameObject Fridge;
    public GameObject Crowbar;
    public GameObject Zapparticle;
    public DogMovement dogMovement;
    public SmoothCamera smoothcamera;



	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == Crowbar)
        {
            dogMovement.whatShouldISay = gameObject;
            smoothcamera.player2 = Fridge.transform;
            dogMovement.StartCoroutine("CameraPanObject");
            Zapparticle.GetComponent<Animator>().SetBool("SparkTrigger",true);
        }
           

    }
}
