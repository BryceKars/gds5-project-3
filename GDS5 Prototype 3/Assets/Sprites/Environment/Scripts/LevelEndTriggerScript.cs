﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelEndTriggerScript : MonoBehaviour {

    public GameObject FinishText;
    public GameObject WhiteOverlay;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == 10)
        {
            WhiteOverlay.GetComponent<Animator>().SetTrigger("EndTrig");
            StartCoroutine(RestartGame());
        }

    }

    IEnumerator RestartGame()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(0);
    }
}
