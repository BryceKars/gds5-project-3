﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransition : MonoBehaviour {

    public int WhichLevel;
    public bool LockedLevel;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (LockedLevel == true)
        {
            gameObject.GetComponent<Button>().interactable = false;
        }

        if (PlayerPrefs.HasKey("Lv1Unlocked"))
        {
            gameObject.GetComponent<Button>().interactable = true;
        }
	}

    public void LoadNewScene()
    {
        SceneManager.LoadScene(WhichLevel);
    }
}
