﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDeadZoneTrigger : MonoBehaviour {

    public bool MoveDisabled;

    public LayerMask MouseOnly;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity,MouseOnly);

        if ((hit.collider != null))
        {
            Debug.Log("AHHHHHHH");
            MoveDisabled = true;
        }
        else
        {
            MoveDisabled = false;
        }
    }
}
