﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour {

    public GameObject PossessParticle;
    public GhostPos ghostPos;
    public Material OverlayMat;
    public Material DefaultMat;

    public ParticleSystem PS;

    // Use this for initialization
    void Start ()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Possess" && other.gameObject != null && other.gameObject.name != "Dog")
        {
            PS.Play();
            other.gameObject.GetComponent<SpriteRenderer>().material = OverlayMat;

        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if ((other.gameObject.tag == "Possess" || other.gameObject.tag == "Possessed") && other.gameObject != null && other.gameObject.name != "Dog")
        {

            other.gameObject.GetComponent<SpriteRenderer>().material = DefaultMat;
            PS.Stop();
        }
    }

}
