﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPossess : MonoBehaviour {

    public Transform MouseMarker;

    public StaticObjectScript staticObjectScript;

    private Vector2 velocity = Vector2.zero;

    public GameObject HookPulleyBottom;

    public GameObject PulledObj;

    public GameObject Hook;

    public bool Connected;

    public bool Pullable;

    public GameObject ConnectedBarrel;

    public bool BarrelInPlace;




    // Use this for initialization
    void Start ()
    {
        Connected = false;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Mouse1) && (gameObject.tag == "Possessed") && (staticObjectScript.GetComponent<StaticObjectScript>().StaticTriggered == false))
        {
            transform.position = Vector2.SmoothDamp(transform.position, MouseMarker.transform.position, ref velocity, 1f, 7f, Time.deltaTime);

        }

        if (Pullable == true)
        {
            if (gameObject.name == PulledObj.gameObject.name && staticObjectScript.InValve != true)
            {
                if (Connected == true)
                {
                    gameObject.transform.SetParent(HookPulleyBottom.transform);
                    gameObject.transform.localPosition = Vector3.zero;
                    gameObject.transform.localRotation = Quaternion.identity;
                    GetComponent<Rigidbody2D>().isKinematic = true;
                    GetComponent<Rigidbody2D>().useFullKinematicContacts = true;
                    //GetComponent<Collider2D>().enabled = false;
                    //Physics2D.IgnoreCollision(Hook.GetComponent<Collider2D>(), GetComponent<Collider2D>(), true);
                }

                if (Connected == false)
                {
                    //StartCoroutine(HookDisable());
                    HookPulleyBottom.transform.DetachChildren();
                    GetComponent<Rigidbody2D>().isKinematic = false;
                    //GetComponent<Collider2D>().enabled = true;
                    //Physics2D.IgnoreCollision(Hook.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
                }
            }
        }
        else
        {
            return;
        }
        

       


    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (Pullable == true)
        {
            if (other.gameObject.name == Hook.gameObject.name)
            {
                Connected = true;
            }
        }
        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject == ConnectedBarrel)
        {
            BarrelInPlace = true;
        }

        if (Pullable == true)
        {
            if (other.gameObject.name == Hook.gameObject.name)
            {
                Connected = true;
            }
        }
    }

    IEnumerator HookDisable()
    {
        if (Connected == false)
        {
            HookPulleyBottom.GetComponent<Collider2D>().enabled = false;
            yield return new WaitForSeconds(1f);
            HookPulleyBottom.GetComponent<Collider2D>().enabled = true;
        }
    }

}
