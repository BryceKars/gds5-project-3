﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulleyPossess : MonoBehaviour {

    public float PullSpeed;

    public AudioSource PulleySource;
    public AudioClip PulleyClip;

	// Use this for initialization
	void Start ()
    {
        PulleySource.clip = PulleyClip;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (gameObject.tag == "Possess")
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }

        if (gameObject.tag == "Untagged")
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.black;
        }

        if ((Input.GetKey(KeyCode.Mouse1)) && (Input.GetAxis("Mouse Y") > 0) && gameObject.tag == "Possessed")
        {
            Debug.Log("You MPOVE UP");
            gameObject.GetComponent<DistanceJoint2D>().distance -= PullSpeed;

        }

        if ((Input.GetKey(KeyCode.Mouse1)) && (Input.GetAxis("Mouse Y") < 0) && gameObject.tag == "Possessed")
        {
            Debug.Log("You MPOVE DOWN");
            gameObject.GetComponent<DistanceJoint2D>().distance += PullSpeed;
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && gameObject.tag == "Possessed")
        {
            PulleySource.Play();
        }

        if (Input.GetKeyUp(KeyCode.Mouse1) && gameObject.tag == "Possessed")
        {
            PulleySource.Stop();
        }


    }
}
