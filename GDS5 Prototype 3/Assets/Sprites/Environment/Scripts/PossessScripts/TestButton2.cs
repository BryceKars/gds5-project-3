﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestButton2 : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Possessed")
        {
            gameObject.GetComponent<Animator>().SetBool("ButtonTriggered", true);
            GameObject.Find("TestPulley3").gameObject.tag = "Possess";
            GameObject.Find("TestPulley4").gameObject.tag = "Possess";
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Possessed")
        {
            gameObject.GetComponent<Animator>().SetBool("ButtonTriggered", false);
            GameObject.Find("TestPulley3").gameObject.tag = "Untagged";
            GameObject.Find("TestPulley4").gameObject.tag = "Untagged";
        }

    }
}
