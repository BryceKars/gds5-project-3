﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestButtonPossess : MonoBehaviour {

    public AudioSource ButtonSource;
    public AudioClip ButtonClip;
    public AudioClip ButoffClip;

    public GameObject Pulley1;
    public GameObject Pulley2;

	// Use this for initialization
	void Start ()
    {
        ButtonSource.clip = ButtonClip;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
    }

    private void OnCollisionStay2D (Collision2D other)
    {
        if (other.gameObject.tag == "Possessed")
        {
            gameObject.GetComponent<Animator>().SetBool("ButtonTriggered", true);
            Pulley1.tag = "Possess";
            Pulley2.tag = "Possess";
            ButtonSource.Play();
           

         
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Possessed")
        {
            gameObject.GetComponent<Animator>().SetBool("ButtonTriggered", false);
            GameObject.Find("TestPulley1").gameObject.tag = "Untagged";
            GameObject.Find("TestPulley2").gameObject.tag = "Untagged";
            ButtonSource.PlayOneShot(ButoffClip);
            ButtonSource.Stop();
        }
            
    }
}
