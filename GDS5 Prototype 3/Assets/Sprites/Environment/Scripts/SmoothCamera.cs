﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour {


    public Transform player1;
    public Transform player2;
    public Transform newPlayer2;
    private float velocity = 0f;

    private const float DISTANCE_MARGIN = 1.0f;

    private Vector3 middlePoint;
    private Vector3 velocityref = Vector3.zero;
    private float distanceFromMiddlePoint;
    public float distanceBetweenPlayers;
    private float fov;

    void Update()
    {
        

        // Position the camera in the center.
        Vector3 newCameraPos = Camera.main.transform.position;
        newCameraPos.x = middlePoint.x;
        Camera.main.transform.position = newCameraPos;

        // Find the middle point between players.
        Vector3 vectorBetweenPlayers = player2.position - player1.position;
        middlePoint = player1.position + 0.5f * vectorBetweenPlayers;
        Camera.main.orthographicSize = Mathf.SmoothDamp(Camera.main.orthographicSize,(distanceBetweenPlayers /3f) + 8f, ref velocity, 0.5f);

        if (Camera.main.orthographicSize > 11f)
        {
            Camera.main.orthographicSize = 11f;
        }

        // Calculate the new distance.
        distanceBetweenPlayers = vectorBetweenPlayers.magnitude;

        // Set camera to new position.
        Vector3 dir = (Camera.main.transform.position - middlePoint).normalized;
        Camera.main.transform.position = Vector3.SmoothDamp(transform.position,(middlePoint + dir + new Vector3(0,1,0)),ref velocityref,1f);

       
    }
}
