﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObjectScript : MonoBehaviour
{

    public GameObject PossessTarget;
    public GameObject StaticObjectLocation;
    public bool StaticTriggered;

    public bool InValve;

    public MovingPossess movingPossess;

    // Use this for initialization
    void Start()
    {
        StaticTriggered = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == PossessTarget.name)
        {
            if (other.gameObject.name == "Ramp" && movingPossess.BarrelInPlace == true)
            {
                other.GetComponent<Transform>().position = StaticObjectLocation.transform.position;
                other.GetComponent<Transform>().rotation = StaticObjectLocation.transform.rotation;
                other.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
                other.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                gameObject.GetComponent<Collider2D>().enabled = false;
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
                other.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
                gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                
                StaticTriggered = true;
                

                other.GetComponent<PlatformEffector2D>().enabled = true;
                other.GetComponent<Collider2D>().usedByEffector = true;
            }

            if (other.gameObject.name == "Crowbar")
            {
                other.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                other.gameObject.transform.SetParent(gameObject.transform, false);
                gameObject.transform.rotation = Quaternion.Euler(0, 0, -45);
                other.gameObject.transform.localPosition = new Vector3(0, 3, 0);
                other.gameObject.transform.localRotation = Quaternion.Euler(0,0,45);
                other.gameObject.transform.localScale = new Vector3(2f, 2f, 2f);
                other.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
                other.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
                other.gameObject.GetComponent<Rigidbody2D>().useFullKinematicContacts = true;
                StaticTriggered = true;
                InValve = true;

            }
           
        }
        else
        {
            return;
        }
    }
}