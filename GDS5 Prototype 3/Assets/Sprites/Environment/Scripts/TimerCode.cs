﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerCode : MonoBehaviour {


    bool pressed; // Checks if you pressed the button.
    public float timer; // The default value of the timer
    private float curTimer; //The current value of the timer, will be reset based on default value
    public float sensitivity; // How fast must you let go of the mouse to register a click.

    public MovingPossess movingPossess;

	
	void Start () {
        ResetTimer();
	}

    
    void Update() {

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            // Check if the button is pressed down
            pressed = true;
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            /*
             * As soon as you dont hold the button, everything gets cancelled and reset
             * if you let go fast enough based on sensitivity, it will do the click command.
             * It will always reset the timer so you can do this over and over again.
             */
            pressed = false;
            if (curTimer > (timer-sensitivity))
            {
                DoClick();
            }
            ResetTimer();
        }

        if (pressed)
        {
            /*
             * Now this one chcecks if you've pressed the button. It'll countdown before it does the hold and drag
             * if you let go, pressed will be false so this doesnt run anymore. 
             */
            curTimer -= Time.deltaTime;
            if (curTimer <= 0)
            {
                DoHold();
            }
        }

	}

    void ResetTimer() // does as the name implies
    {
        curTimer = timer;
    }

    void DoClick()
    {
        movingPossess.Connected = false;
        //Whatever you want with click
        Debug.Log("Clicc");
    }

    void DoHold()
    {
        //Whatever you want when you hold it
        Debug.Log("Hold");
    }
}
